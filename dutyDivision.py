# imports for working with excel
from openpyxl import load_workbook
import openpyxl

MONTH = "07"
YEAR = "2023"

employees = [
    "SANKAR DA",
    "VIJAY",
    "MOLAY",
    "MUMTAZ",
    "AMIT",
    "DIPTI",
    "DEBAJIT",
    "MANOJ",
    "RONY",
    "ANUJ",
]

# Duites count - [general, calcutta, weekly off, public holiday]
AUDIT_EMPLOYEES = ["DEBAJIT", "SANKAR DA", "MUMTAZ"]
Audit_duities_count = [[0, 0, 0, 0] for i in range(len(AUDIT_EMPLOYEES))]
ACCOUNTS_EMPLOYEES = ["MANOJ", "AMIT", "VIJAY"]
Accounts_duities_count = [[0, 0, 0, 0] for i in range(len(ACCOUNTS_EMPLOYEES))]
GATE_EMPlOYEES = ["MOLAY", "RONY", "DIPTI", "ANUJ"]
Gate_duities_count = [[0, 0, 0, 0] for i in range(len(GATE_EMPlOYEES))]

duty_types = ["AUDIT", "A/C PREPARATION", "GATE"]
racing_days = ["GENERAL", "CALCUTTA", "WEEKLY OFF", "PUBLIC HOLIDAY"]
audit_employees_absent = [[] for i in range(len(AUDIT_EMPLOYEES))]
accounts_employees_absent = [[] for i in range(len(ACCOUNTS_EMPLOYEES))]
gate_employees_absent = [[] for i in range(len(GATE_EMPlOYEES))]

audit_absent_count = [[0, 0, 0, 0] for i in range(len(AUDIT_EMPLOYEES))]
accounts_absent_count = [[0, 0, 0, 0] for i in range(len(ACCOUNTS_EMPLOYEES))]
gate_absent_count = [[0, 0, 0, 0] for i in range(len(GATE_EMPlOYEES))]
# Path: Racing Fixture.xlsx
racing_fixture = []
PUBLIC_HOLIDAYS = [
    29,
]

"""
There is an excel sheet "Racing Fixture.xlsx" having four columns - Sl. No., Day, Date, Centre
The date is in format 1st, 2nd, etc.
3 types of duties are there - Audit, A/C Preparation, Gate
There are 4 types of racing days: General(all centres except calcutta), Calcutta, Weekly Off( Sunday and monday), Public holiday
Duty has to assigned to the employees so that every employee gets equal or near equal number of duties and also equal or near equal number of types of racing days.
duty types need not be equal for every employee.
"""


# input the day numbers on which employees are absent
def input_absent_days():
    global audit_employees_absent
    global accounts_employees_absent
    global gate_employees_absent

    print(
        "Enter the day numbers on which employees are absent.\n If employee A is absent on 1st and 2nd, enter 1 2\nIf no employee is absent on a day, press enter."
    )

    for i in range(len(AUDIT_EMPLOYEES)):
        audit_employees_absent[i] = list(
            map(
                int,
                input(
                    "Enter the day numbers on which "
                    + AUDIT_EMPLOYEES[i]
                    + " will be absent: "
                ).split(),
            )
        )

    for i in range(len(ACCOUNTS_EMPLOYEES)):
        accounts_employees_absent[i] = list(
            map(
                int,
                input(
                    "Enter the day numbers on which "
                    + ACCOUNTS_EMPLOYEES[i]
                    + " will be absent: ",
                ).split(),
            )
        )

    for i in range(len(GATE_EMPlOYEES)):
        gate_employees_absent[i] = list(
            map(
                int,
                input(
                    "Enter the day numbers on which "
                    + GATE_EMPlOYEES[i]
                    + " will be absent: ",
                ).split(),
            )
        )


input_absent_days()

"""
How to determine racing day type?
If the centre is calcutta, it is calcutta day racing_days[1]
If the day is sunday or monday, it is weekly off racing_days[2]
If the day is a public holiday, it is public holiday racing_days[3]
Else it is general racing_days[0]
"""


# read racing fixture from excel
def read_racing_fixture():
    global racing_fixture

    wb = load_workbook("Racing Fixture.xlsx")
    ws = wb.worksheets[0]
    for row in ws.iter_rows():
        racing_fixture.append(row)

    # remove the st, nd,rd,th from the date

    for i in range(len(racing_fixture)):
        racing_fixture[i] = list(racing_fixture[i])
        racing_fixture[i][2] = int(racing_fixture[i][2].value[:-2])
        racing_fixture[i][1], racing_fixture[i][3] = (
            racing_fixture[i][1].value.lower(),
            racing_fixture[i][3].value.lower(),
        )
        racing_fixture[i] = tuple(racing_fixture[i])

        # add a fifth column to racing_fixture to store racing day type
        racing_fixture[i] = racing_fixture[i] + (None,)
        if racing_fixture[i][3] == "calcutta":
            racing_fixture[i] = racing_fixture[i][:4] + (racing_days[1],)
        elif racing_fixture[i][1] == "sunday" or racing_fixture[i][1] == "monday":
            racing_fixture[i] = racing_fixture[i][:4] + (racing_days[2],)
        elif racing_fixture[i][2] in PUBLIC_HOLIDAYS:
            racing_fixture[i] = racing_fixture[i][:4] + (racing_days[3],)
        else:
            racing_fixture[i] = racing_fixture[i][:4] + (racing_days[0],)


read_racing_fixture()
"""
Make an excel file "Duty Division.xlsx" having 8 columns - Sl. No., Day, Date,Centre, Racing Day, Audit, A/C Preparation, Gate
The last 3 columns will have the name of the employee assigned to that duty.
"""


# Path: Duty Division.xlsx
# make a new excel file
wb = openpyxl.Workbook()
ws = wb.create_sheet("Sheet1", 0)

# write header
ws.append(
    (
        "Sl. No.",
        "Day",
        "Date",
        "Centre",
        "Racing Day",
        "Audit",
        "A/C Preparation",
        "Gate",
    )
)

"""
exmaple: employees 0, 1 and 2 in Audit, A/C Preparation and Gate respectively on the first General racing day
employees 3, 4 and 5 in Audit, A/C Preparation and Gate respectively on the second general racing day
and so on

exmaple: employees 0, 1 and 2 in Audit, A/C Preparation and Gate respectively on the first Calcutta racing day
employees 3, 4 and 5 in Audit, A/C Preparation and Gate respectively on the second Calcutta racing day
and so on

exmaple: employees 0, 1 and 2 in Audit, A/C Preparation and Gate respectively on the first Weekly Off racing day
employees 3, 4 and 5 in Audit, A/C Preparation and Gate respectively on the second Weekly Off racing day
and so on

exmaple: employees 0, 1 and 2 in Audit, A/C Preparation and Gate respectively on the first Public Holiday racing day
employees 3, 4 and 5 in Audit, A/C Preparation and Gate respectively on the second Public Holiday racing day
and so on
"""


# allot duties in above fashion and write to excel
def allot_duties():
    global racing_fixture
    global employees
    global ACCOUNTS_EMPLOYEES
    global AUDIT_EMPLOYEES
    global GATE_EMPlOYEES
    global duty_types
    global racing_days
    global ws

    General = 0
    Calcutta = 1
    Weekly_Off = 2
    Public_Holiday = 3

    # write to excel
    for i in range(len(racing_fixture)):
        if racing_fixture[i][4] == "GENERAL":
            audit_idx = General % len(AUDIT_EMPLOYEES)
            accounts_idx = General % len(ACCOUNTS_EMPLOYEES)
            gate_idx = General % len(GATE_EMPlOYEES)

            while racing_fixture[i][2] in audit_employees_absent[audit_idx]:
                audit_idx = (audit_idx + 1) % len(AUDIT_EMPLOYEES)
                audit_absent_count[audit_idx][0] += 1

            while racing_fixture[i][2] in accounts_employees_absent[accounts_idx]:
                accounts_idx = (accounts_idx + 1) % len(ACCOUNTS_EMPLOYEES)
                accounts_absent_count[accounts_idx][0] += 1

            while racing_fixture[i][2] in gate_employees_absent[gate_idx]:
                gate_idx = (gate_idx + 1) % len(GATE_EMPlOYEES)
                gate_absent_count[gate_idx][0] += 1

            ws.append(
                (
                    racing_fixture[i][0].value,
                    racing_fixture[i][1],
                    str(racing_fixture[i][2]) + "/" + MONTH + "/" + YEAR,
                    racing_fixture[i][3],
                    racing_fixture[i][4],
                    AUDIT_EMPLOYEES[audit_idx],
                    ACCOUNTS_EMPLOYEES[accounts_idx],
                    GATE_EMPlOYEES[gate_idx],
                )
            )
            Audit_duities_count[audit_idx][0] += 1
            Accounts_duities_count[accounts_idx][0] += 1
            Gate_duities_count[gate_idx][0] += 1
            General += 1

        elif racing_fixture[i][4] == "CALCUTTA":
            audit_idx = Calcutta % len(AUDIT_EMPLOYEES)
            accounts_idx = Calcutta % len(ACCOUNTS_EMPLOYEES)
            gate_idx = Calcutta % len(GATE_EMPlOYEES)

            while racing_fixture[i][2] in audit_employees_absent[audit_idx]:
                audit_idx = (audit_idx + 1) % len(AUDIT_EMPLOYEES)
                audit_absent_count[audit_idx][1] += 1

            while racing_fixture[i][2] in accounts_employees_absent[accounts_idx]:
                accounts_idx = (accounts_idx + 1) % len(ACCOUNTS_EMPLOYEES)
                accounts_absent_count[accounts_idx][1] += 1

            while racing_fixture[i][2] in gate_employees_absent[gate_idx]:
                gate_idx = (gate_idx + 1) % len(GATE_EMPlOYEES)
                gate_absent_count[gate_idx][1] += 1

            ws.append(
                (
                    racing_fixture[i][0].value,
                    racing_fixture[i][1],
                    str(racing_fixture[i][2]) + "/" + MONTH + "/" + YEAR,
                    racing_fixture[i][3],
                    racing_fixture[i][4],
                    AUDIT_EMPLOYEES[audit_idx],
                    ACCOUNTS_EMPLOYEES[accounts_idx],
                    GATE_EMPlOYEES[gate_idx],
                )
            )
            Audit_duities_count[audit_idx][1] += 1
            Accounts_duities_count[accounts_idx][1] += 1
            Gate_duities_count[gate_idx][1] += 1
            Calcutta += 1

        elif racing_fixture[i][4] == "WEEKLY OFF":
            audit_idx = Weekly_Off % len(AUDIT_EMPLOYEES)
            accounts_idx = Weekly_Off % len(ACCOUNTS_EMPLOYEES)
            gate_idx = Weekly_Off % len(GATE_EMPlOYEES)

            while racing_fixture[i][2] in audit_employees_absent[audit_idx]:
                audit_idx = (audit_idx + 1) % len(AUDIT_EMPLOYEES)
                audit_absent_count[audit_idx][2] += 1

            while racing_fixture[i][2] in accounts_employees_absent[accounts_idx]:
                accounts_idx = (accounts_idx + 1) % len(ACCOUNTS_EMPLOYEES)
                accounts_absent_count[accounts_idx][2] += 1

            while racing_fixture[i][2] in gate_employees_absent[gate_idx]:
                gate_idx = (gate_idx + 1) % len(GATE_EMPlOYEES)
                gate_absent_count[gate_idx][2] += 1

            ws.append(
                (
                    racing_fixture[i][0].value,
                    racing_fixture[i][1],
                    str(racing_fixture[i][2]) + "/" + MONTH + "/" + YEAR,
                    racing_fixture[i][3],
                    racing_fixture[i][4],
                    AUDIT_EMPLOYEES[audit_idx],
                    ACCOUNTS_EMPLOYEES[accounts_idx],
                    GATE_EMPlOYEES[gate_idx],
                )
            )
            Audit_duities_count[audit_idx][2] += 1
            Accounts_duities_count[accounts_idx][2] += 1
            Gate_duities_count[gate_idx][2] += 1

            Weekly_Off += 1

        elif racing_fixture[i][4] == "PUBLIC HOLIDAY":
            audit_idx = Public_Holiday % len(AUDIT_EMPLOYEES)
            accounts_idx = Public_Holiday % len(ACCOUNTS_EMPLOYEES)
            gate_idx = Public_Holiday % len(GATE_EMPlOYEES)

            while racing_fixture[i][2] in audit_employees_absent[audit_idx]:
                audit_idx = (audit_idx + 1) % len(AUDIT_EMPLOYEES)
                audit_absent_count[audit_idx][3] += 1

            while racing_fixture[i][2] in accounts_employees_absent[accounts_idx]:
                accounts_idx = (accounts_idx + 1) % len(ACCOUNTS_EMPLOYEES)
                accounts_absent_count[accounts_idx][3] += 1

            while racing_fixture[i][2] in gate_employees_absent[gate_idx]:
                gate_idx = (gate_idx + 1) % len(GATE_EMPlOYEES)
                gate_absent_count[gate_idx][3] += 1

            ws.append(
                (
                    racing_fixture[i][0].value,
                    racing_fixture[i][1],
                    str(racing_fixture[i][2]) + "/" + MONTH + "/" + YEAR,
                    racing_fixture[i][3],
                    racing_fixture[i][4],
                    AUDIT_EMPLOYEES[audit_idx],
                    ACCOUNTS_EMPLOYEES[accounts_idx],
                    GATE_EMPlOYEES[gate_idx],
                )
            )
            Audit_duities_count[audit_idx][3] += 1
            Accounts_duities_count[accounts_idx][3] += 1
            Gate_duities_count[gate_idx][3] += 1
            Public_Holiday += 1

    wb.save("Duty Division.xlsx")


allot_duties()


# print the number of duties each employee in table format
def print_duties():
    global Audit_duities_count
    global Accounts_duities_count
    global Gate_duities_count

    print(f"Employee\tGen\tCal\tWeekly\tHoliday\tTotal")

    print("\t\t\tAUDIT")
    print("-" * 50)

    for i in range(len(AUDIT_EMPLOYEES)):
        a, b, c, d = (
            Audit_duities_count[i][0],
            Audit_duities_count[i][1],
            Audit_duities_count[i][2],
            Audit_duities_count[i][3],
        )
        print(f"{AUDIT_EMPLOYEES[i]}\t\t{a}\t{b}\t{c}\t{d}\t{a+b+c+d}")

    print("\t\t\tACCOUNTS")
    print("-" * 50)
    for i in range(len(ACCOUNTS_EMPLOYEES)):
        a, b, c, d = (
            Accounts_duities_count[i][0],
            Accounts_duities_count[i][1],
            Accounts_duities_count[i][2],
            Accounts_duities_count[i][3],
        )
        print(f"{ACCOUNTS_EMPLOYEES[i]}\t\t{a}\t{b}\t{c}\t{d}\t{a+b+c+d}")

    print("\t\t\tGATE")
    print("-" * 50)
    for i in range(len(GATE_EMPlOYEES)):
        a, b, c, d = (
            Gate_duities_count[i][0],
            Gate_duities_count[i][1],
            Gate_duities_count[i][2],
            Gate_duities_count[i][3],
        )
        print(f"{GATE_EMPlOYEES[i]}\t\t{a}\t{b}\t{c}\t{d}\t{a+b+c+d}")


print_duties()

# save the file
wb.save("Duty Division.xlsx")

# close the file
wb.close()
